# NI-VEM semester project: "Zatiženost technické menzy vůci času"

## Data

General info:

- **Source:** CTU datacenter + Technical canteen
- **Consists of 5 .csv files**: 4 are by year and one is concatenated.
- **Column separator: ";"**
- **Name format**: Stat-TM-YYYY, where YYYY is a year. Stat-TM is the concatenated one.

One row represents one checkout and consists of:

- date in format *YYYY-MM-DD*
- time in format *HH:MM:SS*
- number of unit (*P1-P4*)

## Results

**Data nalysis:**

- **All key points are finished**

**Supplements:**

- mind map (**Done**)
- documentation (*//TODO//*)
- poster (*//TODO//*)

## Sources

Data are obtained from CTU data center after approval of the canteen.

## Authors

Oleksandr Korotetskyi (korotole@fit.cvut.cz)

Iaroslav Kolodka (kolodiar@fit.cvut.cz)

Maksim Shchukin (shchumak@fit.cvut.cz)

Oleh Kuznetsov (kuzneole@fit.cvut.cz)
